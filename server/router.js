const Authentication = require('./controllers/authentication');
const Profile = require('./controllers/userinfo');
const StreamLinks = require('./controllers/stream');

// service
const passport = require('passport');
const passportService = require('./services/passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function(app) {

  app.get('/api/', requireAuth, function(req, res) {
    res.send({ message: 'Super secret code is ABC123' });
  });

  app.post('/api/signup', Authentication.signup);

  app.post('/api/signin', requireSignin, Authentication.signin);

  app.get('/api/verify_jwt', requireAuth, Authentication.verifyJwt);

  app.get('/api/profile', requireAuth, Profile.fetchProfile);

  app.put('/api/profile', requireAuth, Profile.updateProfile);

  app.put('/api/password', requireAuth, Profile.resetPassword);

  app.get('/api/streamlinks', StreamLinks.fetchStreams);

  app.post('/api/streamlinks', requireAuth, StreamLinks.createStreamLink);
  
  app.get('/api/streamlinks/:id', StreamLinks.fetchStreamLink);

  app.put('/api/streamlinks/:id', requireAuth, StreamLinks.updateLink);

  app.get('/api/allow_edit_or_delete/:id', requireAuth, StreamLinks.allowUpdateOrDelete);

  app.delete('/api/streamlinks/:id', requireAuth, StreamLinks.deleteLink);

  app.get('/api/my_links', requireAuth, StreamLinks.fetchStreamLinkssByUserId);

};
