const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const streamlinkSchema = new Schema({
  title: String,
  link: String,
  authorId: String,
  authorName: String,
  time: Date,
});

const ModelClass = mongoose.model('streamlink', streamlinkSchema);

module.exports = ModelClass;