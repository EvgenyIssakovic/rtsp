let _ = require('lodash');

const StreamLink = require('../models/streamlink');

/**
 * ------- Stream Link APIs -------
 */

/**
 * Get a list of Stream Links
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchStreams = function (req, res, next) {
    StreamLink
        .find({})
        .select({})
        .limit(100)
        .sort({
            time: -1
        })
        .exec(function (err, links) {
            if (err) {
                console.log(err);
                return res.status(422).json({
                    message: 'Error! Could not retrieve links.'
                });
            }
            res.json(links);
        });
};

/**
 * Create a new Stream Link
 *
 * @param req
 * @param res
 * @param next
 */
exports.createStreamLink = function (req, res, next) {
    const user = req.user;

    const title = req.body.title;
    const link = req.body.link;
    const authorId = user._id;
    const authorName = user.firstName + " " + user.lastName;

    const time = Date.now();

    if (!title || !link) {
        return res.status(422).json({
            message: 'Title, and link are all required.'
        });
    }

    const streamlink = new StreamLink({
        title: title,
        link: link,
        authorId: authorId,
        authorName: authorName,
        time: time,
    });

    streamlink.save(function (err, link) {  
        if (err) {
            return next(err);
        }
        res.json(link); 
    });
};

/**
 * Fetch a single Stream Links by Stream Link ID
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchStreamLink = function (req, res, next) {
    StreamLink.findById({
        _id: req.params.id
    }, function (err, link) {
        if (err) {
            console.log(err);
            return res.status(422).json({
                message: 'Error! Could not retrieve the link with the given link ID.'
            });
        }
        if (!link) {
            return res.status(404).json({
                message: 'Error! The link with the given ID is not exist.'
            });
        }
        res.json(link);
        debugger;
    });
};

/**
 * Check if current link can be updated or deleted by the authenticated user: The author can only make change to his/her own links
 *
 * @param req
 * @param res
 * @param next
 */
exports.allowUpdateOrDelete = function (req, res, next) {

    const user = req.user;

    StreamLink.findById({
        _id: req.params.id
    }, function (err, link) {

        if (err) {
            console.log(err);
            return res.status(422).json({
                message: 'Error! Could not retrieve the link with the given link ID.'
            });
        }

        if (!link) {
            return res.status(404).json({
                message: 'Error! The link with the given ID does not exist.'
            });
        }

        if (!user._id.equals(link.authorId)) {
            return res.send({ allowChange: false });
        }
        res.send({ allowChange: true });
    });
};

/**
 * Edit/Update a link
 *
 * @param req
 * @param res
 * @param next
 */
exports.updateLink = function (req, res, next) {
    const user = req.user;
    StreamLink.findById({
        _id: req.params.id
    }, function (err, rtspLink) {

        if (err) {
            console.log(err);
            return res.status(422).json({
                message: 'Error! Could not retrieve the link with the given link ID.'
            });
        }

        if (!rtspLink) {
            return res.status(404).json({
                message: 'Error! The rtsp link with the given ID does not exist.'
            });
        }

        if (!user._id.equals(rtspLink.authorId)) {
            return res.status(422).json({
                message: 'Error! You have no authority to modify this link.'
            });
        }

        const title = req.body.title;
        const link = req.body.link;

        if (!title || !link) {
            return res.status(422).json({
                message: 'Title, link are all required.'
            });
        }

        rtspLink.title = title;
        rtspLink.link = link;

        rtspLink.save(function (err, rtspLink) {  
            if (err) {
                return next(err);
            }
            res.json(rtspLink); 
        });
    });
};

/**
 * Delete a link by link ID
 *
 * @param req
 * @param res
 * @param next
 */
exports.deleteLink = function (req, res, next) {

    StreamLink.findByIdAndRemove(req.params.id, function (err, link) {
        if (err) {
            return next(err);
        }
        if (!link) {
            return res.status(422).json({
                message: 'Error! The rtsp link with the given ID does not exist.'
            });
        }

        res.json({
            message: 'The rtsp link has been deleted successfully!'
        });
    });
};

/**
 * Fetch links by author ID
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchStreamLinkssByUserId = function (req, res, next) {

    const user = req.user;
    StreamLink
        .find({
            authorId: user._id
        })
        .select({})
        .limit(100)
        .sort({
            time: -1
        })
        .exec(function (err, links) {
            if (err) {
                console.log(err);
                return res.status(422).json({
                    message: 'Error! Could not retrieve links.'
                });
            }
            res.json(links);
        });
};
