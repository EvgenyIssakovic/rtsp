import _ from 'lodash';
import {
  FETCH_LINKS,
  CREATE_LINK,
  FETCH_STREAM_LINK,
  UPDATE_LINK,
  DELETE_LINK
} from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case FETCH_LINKS:
      return _.mapKeys(action.payload, '_id');
    case CREATE_LINK:
      return { ...state, [action.payload._id]: action.payload };
    case FETCH_STREAM_LINK:
      return { ...state, [action.payload._id]: action.payload }; 
    case UPDATE_LINK:
      return { ...state, [action.payload._id]: action.payload }; 
    case DELETE_LINK:
      return _.omit(state, action.payload);
    default:
      return state;
  }
}