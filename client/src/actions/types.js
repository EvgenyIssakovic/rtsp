export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';

export const FETCH_PROFILE = 'fetch_profile';
export const CLEAR_PROFILE = 'clear_profile';
export const UPDATE_PROFILE = 'update_profile';

export const FETCH_LINKS = 'fetch_links';

export const CREATE_LINK = 'create_link';

export const FETCH_STREAM_LINK = 'fetch_stream_link';

export const UPDATE_LINK = 'update_link';

export const DELETE_LINK = 'delete_link';

export const CHECK_AUTHORITY = 'check_authority';
