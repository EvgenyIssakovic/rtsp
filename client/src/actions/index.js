import axios from 'axios';
import { reset } from 'redux-form';
import {
  AUTH_USER,
  UNAUTH_USER,
  FETCH_PROFILE,
  CLEAR_PROFILE,
  UPDATE_PROFILE,
  DELETE_LINK,
  FETCH_LINKS,
  FETCH_STREAM_LINK,
  UPDATE_LINK,
  CREATE_LINK,

  CHECK_AUTHORITY,
} from './types';

const ROOT_URL = '/api';

/**
 * Authentication
 */

export function signinUser({ email, password }, historyPush, historyReplace) {

 
  return function (dispatch) {
    axios.post(`${ROOT_URL}/signin`, { email, password })  
      .then(response => {    
        localStorage.setItem('token', response.data.token);

        dispatch({
          type: AUTH_USER,
          payload: response.data.username,
        });
        historyPush('/streamlinks');
      })
      .catch(() => {  
        historyReplace('/signin', {
          time: new Date().toLocaleString(),
          message: 'The email and/or password are incorrect.'
        });
      });
  }
}

export function signupUser({ email, password, firstName, lastName }, historyPush, historyReplace) {

  return function (dispatch) {

    axios.post(`${ROOT_URL}/signup`, { email, password, firstName, lastName })
      .then(response => {  
        historyPush('/streamlinks', { time: new Date().toLocaleString(), message: response.data.message });
      })
      .catch(({ response }) => {  
        historyReplace('/signup', { time: new Date().toLocaleString(), message: response.data.message });
      });
  }
}

export function signoutUser() {

  localStorage.removeItem('token');
  return { type: UNAUTH_USER };

}

export function verifyJwt() {

  return function (dispatch) {
    axios.get(`${ROOT_URL}/verify_jwt`, {
      headers: { authorization: localStorage.getItem('token') }
    }).then((response) => {
      dispatch({
        type: AUTH_USER,
        payload: response.data.username,
      });
    });
  }
}

/**
 * User information
 */

export function fetchProfile() {

  return function (dispatch) {
    axios.get(`${ROOT_URL}/profile`, {
      headers: { authorization: localStorage.getItem('token') }
    }).then(response => {
      dispatch({
        type: FETCH_PROFILE,
        payload: response.data.user,
      });
    });
  }
}

export function clearProfile() {
  return { type: CLEAR_PROFILE };
}

export function updateProfile({ firstName, lastName, birthday, sex, phone, address, occupation, description }, historyReplace) {

  return function (dispatch) {
    axios.put(`${ROOT_URL}/profile`, {  
      firstName,
      lastName,
      birthday,
      sex,
      phone,
      address,
      occupation,
      description,
    }, { 
        headers: { authorization: localStorage.getItem('token') },  
      }
    )
      .then((response) => {  
       
        dispatch({
          type: UPDATE_PROFILE,
          payload: response.data.user,
        });
      
        dispatch({
          type: AUTH_USER,
          payload: response.data.user.firstName + ' ' + response.data.user.lastName,
        });
      
        historyReplace('/profile', {
          status: 'success',
          time: new Date().toLocaleString(),
          message: 'You have successfully updated your profile.',
        });
      })
      .catch(() => { 
        historyReplace('/profile', {
          status: 'fail',
          time: new Date().toLocaleString(),
          message: 'Update profile failed. Please try again.',
        });
      });
  }
}

export function changePassword({ oldPassword, newPassword }, historyReplace) {

  return function (dispatch) {
    axios.put(`${ROOT_URL}/password`, {
      oldPassword,
      newPassword,
    }, {
        headers: { authorization: localStorage.getItem('token') }, 
      })
      .then((response) => {
        dispatch(reset('settings')); 
        historyReplace('/settings', {
          status: 'success',
          time: new Date().toLocaleString(),
          message: response.data.message,
        });
      })
      .catch(({ response }) => {
        historyReplace('/settings', {
          status: 'fail',
          time: new Date().toLocaleString(),
          message: response.data.message,
        });
      });
  }
}

export function fetchLinks() {

  return function (dispatch) {
    axios.get(`${ROOT_URL}/streamlinks`).then((response) => {
      dispatch({
        type: FETCH_LINKS,
        payload: response.data,
      });
    });
  }
}

export function createStreamLink({ title, link }, historyPush, historyReplace) {

  return function (dispatch) {
    axios.post(`${ROOT_URL}/streamlinks`, {
      title,
      link,
    }, {
        headers: { authorization: localStorage.getItem('token') },  
      })
      .then((response) => {  
        dispatch({
          type: CREATE_LINK,
          payload: response.data,
        });
        historyPush(`/streamlinks/${response.data._id}`);
      })
      .catch(({ response }) => {  
        historyReplace('/streamlinks/new', {
          time: new Date().toLocaleString(),
          message: response.data.message,
        });
      });
  }
}

export function fetchStreamLink(id) {

  return function (dispatch) {
    axios.get(`${ROOT_URL}/streamlinks/${id}`).then(response => {
     
      dispatch({
        type: FETCH_STREAM_LINK,
        payload: response.data,
      });
      debugger;
    });
  }
}

export function updateLink({ _id, title, link }, onEditSuccess, historyReplace) {

  return function (dispatch) {
    axios.put(`${ROOT_URL}/streamlinks/${_id}`, {
      _id,
      title,
      link
    }, {
        headers: { authorization: localStorage.getItem('token') }, 
      })
      .then((response) => {
        dispatch({
          type: UPDATE_LINK,
          payload: response.data,
        });
        onEditSuccess(); 
        historyReplace(`/streamlinks/${_id}`, null);
      })
      .catch(({ response }) => {
        historyReplace(`/streamlinks/${_id}`, {
          time: new Date().toLocaleString(),
          message: response.data.message,
        });
      });
  }
}

export function deleteLink(id, historyPush) {

  return function (dispatch) {
    axios.delete(`${ROOT_URL}/streamlinks/${id}`, {
      headers: { authorization: localStorage.getItem('token') },  
    }).then((response) => {
      dispatch({
        type: DELETE_LINK,
        payload: id,
      });
      historyPush('/streamlinks');
    })
  }
}

export function fetchLinksByUserId() {

  return function (dispatch) {
    axios.get(`${ROOT_URL}/my_links`, {
      headers: { authorization: localStorage.getItem('token') }, 
    })
      .then((response) => {
        dispatch({
          type: FETCH_LINKS,
          payload: response.data,
        });
      });
  }
}




/**
 * Check authority: Check if the user has the authority to make change to a specific link
 */
export function checkAuthority(linkId) {

  return function (dispatch) {
    axios.get(`${ROOT_URL}/allow_edit_or_delete/${linkId}`, {
      headers: { authorization: localStorage.getItem('token') },
    }).then((response) => {
      dispatch({
        type: CHECK_AUTHORITY,
        payload: response.data.allowChange,
      });
    }).catch(() => {  
      dispatch({
        type: CHECK_AUTHORITY,
        payload: false,
      })
    });
  }
}