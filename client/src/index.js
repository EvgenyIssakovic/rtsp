import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import reduxThunk from 'redux-thunk';

import Header from './components/header';
import Footer from './components/footer';
import NoMatch from './components/nomatch';
import Welcome from './components/welcome';
import Signin from './components/auth/signin';
import Signup from './components/auth/signup';
import RequireAuth from './components/auth/require_auth';
import Profile from './components/userinfo/profile';
import Settings from './components/userinfo/settings';
import StreamLinksList from './components/streamlink/stream_links_list';
import CreateStreamLink from './components/streamlink/create_stream_link';
import RTSPLink from './components/streamlink/rtsp/rtsp_link';
import LinksMine from './components/streamlink/user_stream_links';


import reducers from './reducers';
import { AUTH_USER } from './actions/types';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

const token = localStorage.getItem('token');
if (token) {
  store.dispatch({ type: AUTH_USER });
}

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Header />
        <div className="container" id="content">
          <Switch>
            <Route exact path='/' component={Welcome} />
            <Route path='/signin' component={Signin} />
            <Route path='/signup' component={Signup} />
            <Route path="/profile" component={RequireAuth(Profile)} />
            <Route path="/settings" component={RequireAuth(Settings)} />
            <Route exact path='/streamlinks' component={StreamLinksList} />
            <Route path='/streamlinks/new' component={RequireAuth(CreateStreamLink)} />
            <Route path='/streamlinks/:id' component={RTSPLink} />
            <Route path='/my_links' component={RequireAuth(LinksMine)} />
            <Route component={NoMatch} />
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  </Provider>
  , document.getElementById('root')
);