import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <div>
    <div className="jumbotron">
      <h1 className="display-3">Welcome!</h1>
      <p>to view all RTSP videos click below</p>
      <p><Link className="btn btn-primary btn-lg" to="/streamlinks" role="button">All Uploaded links &raquo;</Link></p>
    </div>
  </div>
);