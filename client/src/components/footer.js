import React from 'react';

export default () => {
  return (
    <footer className="footer">
      <div className="container">
        <span className="text-muted">evgeny.kovich@gmail.com - creating apps</span>
      </div>
    </footer>
  );
}