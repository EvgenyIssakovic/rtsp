import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchLinksByUserId } from '../../actions/index';
import StreamLink from '../streamlink/stream_link';

class UserStreamLinks extends Component {

  componentDidMount() {
    this.props.fetchLinksByUserId();
  }
  renderLink(link) {
    return (
      <div key={link._id} className="iframeItem">
        <StreamLink rtspLink={link.link} />
        <h3>
          <Link className="link-without-underline" to={`/streamlinks/${link._id}`}>
            {link.title}
          </Link>
        </h3>

        <span className="span-with-margin text-grey"> • </span>
        <span className="span-with-margin text-grey">{link.authorName}</span>
        <span className="span-with-margin text-grey"> • </span>
        <span className="span-with-margin text-grey">{new Date(link.time).toLocaleString()}</span>

      </div>
    );
  }

  render() {
    return (
      <div>
        <div className="flexBetween margerBottom">
          <h3>{`${this.props.username}'s RTSP Links`}</h3>
          <Link className="btn btn-primary" to={'/streamlinks/new'}>Add New RTSP Link</Link>
        </div>
        <div className="flexStart">
          {_.map(this.props.links, link => {
            return this.renderLink(link);
          })}
        </div>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return { links: state.links, username: state.auth.username, };
}

export default connect(mapStateToProps, { fetchLinksByUserId })(UserStreamLinks);