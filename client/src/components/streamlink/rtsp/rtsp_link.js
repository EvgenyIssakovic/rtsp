import React, { Component } from 'react';
import { connect } from 'react-redux';
import NoMatch from '../../nomatch';
import LinkEdit from '../../streamlink/link_edit';
import StreamLink from '../../streamlink/stream_link';

import { fetchStreamLink, checkAuthority, deleteLink } from '../../../actions';

class RTSPLink extends Component {

    constructor(props) {
        super(props);
        this.state = {
            beingEdit: false
        };
    }
    componentDidMount() {
        this.setState({
            beingEdit: false
        });    
        const { id } = this.props.match.params;
        if (!this.props.link) {
            this.props.fetchStreamLink(id);
        }
        this.props.checkAuthority(id);
    }

    handleEditSuccess() {
        this.setState({
            beingEdit: false
        });
    }

    onEditClick() {
        this.setState({
            beingEdit: true
        });
    }

    onDeleteClick() {
        const { id } = this.props.match.params;
        this.props.deleteLink(id, (path) => {
            this.props.history.push(path);
        });
    }

    renderDeleteConfirmModal() {
        return (
            <div className="modal fade" id="deleteConfirmModal" tabIndex="-1" role="dialog" aria-labelledby="deleteConfirmModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="deleteConfirmModalLabel">Confirm Delete</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>Are you sure you want to delete this RTSP link <strong>Attention!</strong> This delete action is permanent.</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={this.onDeleteClick.bind(this)}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderUpdateAndDeleteButton() {
        if (this.props.allowChange) {
            return (
                <div>
                    <button className="btn btn-primary mr-1" onClick={this.onEditClick.bind(this)}>Edit</button>
                    <button className="btn btn-danger" data-toggle="modal" data-target="#deleteConfirmModal">Delete</button>
                </div>
            );
        }
    }
    renderLink() {
        const { title, _id, authorName, link, time } = this.props.link;
        return (
            <div key={_id} className="iframeItem">
                <StreamLink rtspLink={link} />
                <h3>
                    {title}
                </h3>
                <span className="span-with-margin text-grey"> • </span>
                <span className="span-with-margin text-grey">{authorName}</span>
                <span className="span-with-margin text-grey"> • </span>
                <span className="span-with-margin text-grey">{new Date(time).toLocaleString()}</span>
                <div className="flexEnd margTopSmall">
                    <button className="btn btn-primary mr-1" onClick={this.onEditClick.bind(this)}>Edit</button>
                    <button className="btn btn-danger" data-toggle="modal" data-target="#deleteConfirmModal">Delete</button>
                </div>
            </div>
        );
    }
    render() {
        if (!this.props.link) {
            return <NoMatch />;
        }
        if (this.state.beingEdit) {
            return (
                <LinkEdit
                    link={this.props.link}
                    onEditSuccess={this.handleEditSuccess.bind(this)}
                    history={this.props.history}
                    state={this.props.history.location.state}
                    action={this.props.history.action}
                />
            );
        }    
        return (
            <div className="link">
                {this.renderLink()}
                {this.renderDeleteConfirmModal()}
            </div>
        );
    }
}

function mapStateToProps({ links, auth }, ownProps) {
    return {
        link: links[ownProps.match.params.id],
        allowChange: auth.allowChange,
    };
}

export default connect(mapStateToProps, { fetchStreamLink, checkAuthority, deleteLink })(RTSPLink);