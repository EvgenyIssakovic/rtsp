import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { createStreamLink } from '../../actions';

class CreateStreamLink extends Component {

  handleFormSubmit({ title, link }) {

    this.props.createStreamLink({ title, link }, (path) => {
      this.props.history.push(path);
    }, (path, state) => {
      this.props.history.replace(path, state);
    });
  }

  renderInput = (field) => (
    <fieldset className="form-group">
      <label>{field.label}</label>
      <input
        className="form-control"
        {...field.input}
        type={field.type}
        placeholder={field.placeholder}
        required={field.required ? 'required' : ''}
        disabled={field.disabled ? 'disabled' : ''}
      />
    </fieldset>
  );

  renderTextEditor = (field) => (
    <fieldset className="form-group">
      <label>{field.label}</label>
      <input className="form-control" id="x" type="hidden" name="content" />
      <trix-editor input="x" {...field.input} />
    </fieldset>
  );

  renderAlert() {

    const { state } = this.props.history.location;
    const { action } = this.props.history;

    if (state && action === 'REPLACE') {
      return (
        <div className="alert alert-danger" role="alert">
          <strong>Oops!</strong> {state.message}
        </div>
      );
    }
  }

  render() {

    const { handleSubmit } = this.props;

    return (
      <div className="link">
        {this.renderAlert()}
        <h2 className="mb-5">New RTSP Link</h2>
        <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
          <Field name="title" component={this.renderInput} type="text" label="Title:" placeholder="Enter your title" required={true} />
          <Field name="link" component={this.renderInput} type="text" label="link:" placeholder="Enter your link" required={true} />
          <button action="submit" className="btn btn-primary">Publish</button>
        </form>
      </div>
    );
  }
}

CreateStreamLink = reduxForm({
  form: 'create_stream_link',
})(CreateStreamLink);

export default connect(null, { createStreamLink })(CreateStreamLink);