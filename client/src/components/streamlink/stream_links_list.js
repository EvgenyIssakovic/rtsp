import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchLinks } from '../../actions/index';
import StreamLink from '../streamlink/stream_link';
class StreamLinksList extends Component {

    componentDidMount() {
        this.props.fetchLinks();
       
    }



    renderLinkSummary(link) {
        return (
            <div key={link._id} className="iframeItem">
               <StreamLink  rtspLink={link.link}/>
                <h3>
                    <Link className="link-without-underline" to={`/streamlinks/${link._id}`}>
                        {link.title}
                    </Link>
                </h3>   
                <span className="span-with-margin text-grey"> • </span>
                <span className="span-with-margin text-grey">{link.authorName}</span>
                <span className="span-with-margin text-grey"> • </span>
                <span className="span-with-margin text-grey">{new Date(link.time).toLocaleString()}</span>
            </div>
        );
    }

    render() {
        return (  
            <div>  
                <Link className="btn btn-primary mb-5" to={'/streamlinks/new'}>Add New RTSP Link</Link>           
                <div className="flexStart">
                {_.map(this.props.links, link => {
                    return this.renderLinkSummary(link);
                })}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { links: state.links };
}

export default connect(mapStateToProps, { fetchLinks })(StreamLinksList);