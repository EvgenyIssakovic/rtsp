import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { updateLink } from '../../actions';

class LinkEdit extends Component {



    handleFormSubmit({ title, link }) {

        const _id = this.props.link._id;

        this.props.updateLink({ _id, title, link }, this.props.onEditSuccess, (path, state) => {
            this.props.history.replace(path, state);
        });
    }

    renderInput = (field) => (
        <fieldset className="form-group">
            <label>{field.label}</label>
            <input
                className="form-control"
                {...field.input}
                type={field.type}
                placeholder={field.placeholder}
                required={field.required ? 'required' : ''}
                disabled={field.disabled ? 'disabled' : ''}
            />
        </fieldset>
    );



    renderAlert() {

        const { state } = this.props;
        const { action } = this.props;

        if (state && action === 'REPLACE') {
            return (
                <div className="alert alert-danger" role="alert">
                    <strong>Oops!</strong> {state.message}
                </div>
            );
        }
    }

    render() {

        const { handleSubmit } = this.props;

        return (
            <div className="link">
                {this.renderAlert()}
                <h2 className="mb-5">Edit Your Links</h2>
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                    <Field name="title" component={this.renderInput} type="text" label="Title:" placeholder="Enter your title" required={true} />
                    <Field name="link" component={this.renderInput} type="text" label="link:" placeholder="Enter your link" required={true} />
                    <button action="submit" className="btn btn-primary">Publish</button>
                </form>
            </div>
        );
    }
}

LinkEdit = reduxForm({
    form: 'link_edit',  
})(LinkEdit);

function mapStateToProps(state, ownProps) {
    return { initialValues: ownProps.link };
}

export default connect(mapStateToProps, { updateLink })(LinkEdit);