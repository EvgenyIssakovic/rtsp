import _ from 'lodash';
import React, { Component } from 'react';

class StreamLink extends Component {

    componentDidMount() {

        const s = document.createElement('iframe');
        s.src = 'https://wcs5-eu.flashphoner.com:8888/embed_player?urlServer=wss://wcs5-eu.flashphoner.com:8443&streamName=' + this.props.rtspLink + '&mediaProviders=WebRTC,Flash,MSE,WSPlayer'
        s.frameborder = '0'
        s.width = '350'
        s.height = '300'
        s.scrolling = 'no'
        s.allowfullscreen = 'allowfullscreen'
        this.instance.appendChild(s);
    }

    render() {
        return <div ref={el => (this.instance = el)} />;
    }
}


export default StreamLink;